# Vertex Lit IBL #
Created by [Laurens Mathot](http://codeanimo.com) for use with [PolyWorld](http://forum.unity3d.com/threads/239365-PolyWorld-for-Unity) by [Quantum Theory Entertainment](http://qt-ent.com/).

Vertex Colors set Albedo (and Emission color, in the Emissive shader).

The diffuse IBL cubemap is expected to be convolved in advance.
The reflection cubemap is a regular cubemap, but you can increase the MIP level to approximate rougher surfaces (make sure the cubemap generates MipMaps). This looks best in DX11 mode. In regular mode, you can clearly see the edges of the cubemap faces at the higher MIP levels.

Reflections also work in Vertex Lit mode, but it does show quite significant artifacts when the tessellation of the model is low. In those cases it helps to use a high MIP level.

Support for:
* Lightmapping
* Dynamic Shadows
* Render paths:
* Forward Rendering
* Deferred Lighting
* Vertex Lit

Keep in mind that the Image Based Lighting is not Physically based. Reflections will be added unrealistically to the diffuse (Dynamic AND Image Based) lighting.


Make sure the cginc files stay in the same folder as the shaders themselves.